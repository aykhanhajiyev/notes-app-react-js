import React, {useContext, useState} from 'react';
import {Redirect} from "react-router";
import './Notesapp.scss';
import NotesContext from "./NotesContext";

const NoteForm = (props) => {
    const DEFAULT_COLORS = ["#d5e8d4", "#DAE8FC", "#FEF2CC", "#F8CECC"];
    const [redirect, setRedirect] = useState(false);
    const [noteObj, setNoteObj] = useState({
        title: props.noteObject ? props.noteObject.title : '',
        text: props.noteObject ? props.noteObject.text : '',
        status: props.noteObject ? props.noteObject.status : '',
        color: props.noteObject ? props.noteObject.color : '',
    });
    const [errMessage, seterrMessage] = useState({
        errTitle: props.noteObject ? null : '',
        errText: props.noteObject ? null : ''
    });

    const notesContext = useContext(NotesContext);
    const addNoteHandler = () => {
        //set random color
        let randomColorPosition = Math.round(Math.random() * (DEFAULT_COLORS.length - 1));
        noteObj['color'] = DEFAULT_COLORS[randomColorPosition];
        noteObj['status'] = 'actual';
        notesContext.addNote(noteObj);
        setRedirect(true);
    };

    const formChangeHandler = (e) => {
        let newNoteObj = noteObj;
        newNoteObj[e.target.name] = e.target.value;
        setNoteObj(newNoteObj);

        //error message
        let errMsg = {
            errTitle: "",
            errText: ""
        };
        errMsg.errTitle = (newNoteObj.title.length === 0) ? "Title can't be empty" : null;
        errMsg.errText = (newNoteObj.text.length === 0) ? "Text can't be empty" : null;
        seterrMessage(errMsg);
    };
    const getColor = (e) => {
        noteObj['color'] = e.target.id;
        let allcolordivs = e.currentTarget.children;
        for (let colordiv of allcolordivs) {
            colordiv.classList.remove('color-selected');
        }
        if (e.target.classList.contains('note-form-color')) {
            e.target.classList.add('color-selected');
        }
    };

    const editHandler = () => {
        noteObj['status'] = props.noteObject.status;
        notesContext.updateNote(props.noteObject.id, noteObj)
    };
    return (
        <form className={"note-form"}
              onChange={formChangeHandler}
              onSubmit={props.noteObject ? editHandler : addNoteHandler}>

            <h2 className={"note-form-title"}>
                {props.noteObject ? 'Edit Data' : 'Fill Data'}
            </h2>
            <span className={'note-form-error'}>{errMessage.errTitle}</span>
            <input
                type={"text"}
                placeholder={"Title"}
                className={'note-form-input note-form-input-title'}
                name={"title"}
                defaultValue={noteObj.title}
            />
            <span className={'note-form-error'}>{errMessage.errText}</span>
            <textarea
                name={"text"}
                placeholder={"Text"}
                className={'note-form-input note-form-input-text'}
                defaultValue={noteObj.text}>
            </textarea>

            <div className={'note-form-colorContainer'} onClick={getColor}>
                <label className={'note-form-label'}>Color</label>
                <div
                    className={`note-form-color color-green ${noteObj.color === DEFAULT_COLORS[0] ? 'color-selected' : null}`}
                    id={"0"}></div>
                <div
                    className={`note-form-color color-blue ${noteObj.color === DEFAULT_COLORS[1] ? 'color-selected' : null}`}
                    id={"1"}></div>
                <div
                    className={`note-form-color color-yellow ${noteObj.color === DEFAULT_COLORS[2] ? 'color-selected' : null}`}
                    id={"2"}></div>
                <div
                    className={`note-form-color color-red ${noteObj.color === DEFAULT_COLORS[3] ? 'color-selected' : null}`}
                    id={"3"}></div>
            </div>

            {props.noteObject ? <button type={'submit'}
                                        className={'note-form-btn'}
                                        disabled={(errMessage.errTitle === null && errMessage.errText === null) ? false : true}>Save</button> :
                <button type={'submit'}
                        className={'note-form-btn'}
                        disabled={(errMessage.errTitle === null && errMessage.errText === null) ? false : true}> Create </button>}

            {redirect ? <Redirect to={'/'}/> : null}
        </form>
    );
};

export default NoteForm;