import React from 'react';
import {Link, NavLink} from 'react-router-dom'

const Header = () => {
    return (
        <div className={'header'}>
            <Link to={'/'} className={'header-logo'}>NotesApp</Link>
            <div className={'header-links'}>
                <NavLink to={'/actual'}
                         activeClassName={'header-link-active'}
                         className={'header-link'}>Actual</NavLink>
                <NavLink to={'/archive'}
                         className={'header-link'}
                         activeClassName={'header-link-active'}>Archive</NavLink>
                <NavLink to={'/create'}
                         className={'header-link'}
                         activeClassName={'header-link-active'}>Create</NavLink>

            </div>

        </div>
    );
};

export default Header;