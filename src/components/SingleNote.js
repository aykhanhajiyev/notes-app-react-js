import React, {useContext, useEffect, useState} from 'react';
import {useLocation} from "react-router";
import NotesContext from "./NotesContext";
import Note from "./Note";
import NoteForm from "./NoteForm";
import {Redirect} from "react-router";
import ModalWindow from "./ModalWindow";

const SingleNote = (props) => {
    const [redirect, setRedirect] = useState(false);
    const [noteItem, setNoteItem] = useState({});
    const [isEditMode, setEditMode] = useState(false);
    const [showModal,setshowModal] = useState(false);
    const location = useLocation();
    const noteid = location.pathname.split('/')[2];
    let notesContext = useContext(NotesContext);
    useEffect(() => {
        async function getNote() {
            let note = await notesContext.getNote(noteid);
            await setNoteItem(note);
        }

        getNote()
    }, []);

    const changeEditHandler = ()=>{
        setEditMode(true);
    };

    const deleteNoteHandler = ()=>{
        setshowModal(true);

    };
    const changeStatusHandler = ()=>{
        let updatedNote = noteItem;
        (updatedNote.status==="actual") ? (updatedNote.status="archive") : (updatedNote.status="actual");
        notesContext.updateNote(updatedNote.id,updatedNote);
        setRedirect(true);
    };
    const closeModal = (e)=>{
        if(e.target.classList.contains('modal-bg')){
            setshowModal(false);
        }
    }
    const clickDeleteHandler=()=>{
        //set this note localstorage for restoring
        // localStorage.setItem('deletednote',JSON.stringify(noteItem));
        notesContext.deleteNote(noteid);
        setRedirect(true);
    }
    const cancelBtnHandler=()=>{
        setshowModal(false);
    }
    return (
        <div>
            <div className={'single-note'}>
                {isEditMode ?
                    <NoteForm noteObject={noteItem}/> :
                    <div className='note-viewer'><Note noteObject={noteItem}/>
                        {!isEditMode ? <div className={'buttons'}>
                            <button className={'btn'}
                                    onClick={changeEditHandler}>Edit</button>
                            <button className={'btn'}
                                    onClick={changeStatusHandler}>{(noteItem.status === "actual") ? "Archive" : "Actual"}</button>
                            <button className={'btn'}
                                    onClick={deleteNoteHandler}>Delete</button>
                        </div>: null}

                    </div>}
            </div>

            <ModalWindow showModal={showModal} closeHandler={closeModal} onclickCancelBtn={cancelBtnHandler} onclickOkBtn={clickDeleteHandler}/>
            {redirect ? <Redirect to={'/'}/> : null}
        </div>
    );
};

export default SingleNote;
