import React, {useContext, useEffect, useState} from 'react';
import NotesContext from "./NotesContext";
import Note from "./Note";

const NoteList = (props) => {
    const notesContext = useContext(NotesContext);
    const [noteElements, setElementsToShow] = useState([]);
    useEffect( () => {
        async function fetchNotes() {
            let notesFetched = await notesContext.getNotes();
            let notesToShow = await notesFetched.filter(note => note.status === props.filtering);
            const noteElementsToShow = await notesToShow.map((note, ind) => <Note key={ind} noteObject={note} />);
            await setElementsToShow(noteElementsToShow);

        }
        fetchNotes()
    }, []);
    return (
        <div className={'note-list'}>
            {noteElements}
        </div>
    );
};

export default NoteList;
