import React, {useState} from 'react';
import './ModalWindow.scss';

const ModalWindow = (props) => {
    return (
        (props.showModal) ?
        <div className={'modal-bg'} onClick={(e)=>{props.closeHandler(e)}}>
            <div className={'modal'}>
                <div className={'modal-header'}>Are you sure to delete this note?</div>
                <div className={'modal-content'}>
                    <button className={'modal-btn btn-cancel'} onClick={props.onclickCancelBtn}>Cancel</button>
                    <button className={'modal-btn btn-ok'} onClick={props.onclickOkBtn}>Delete</button>
                </div>
            </div>
        </div> : null
    );
};

export default ModalWindow;