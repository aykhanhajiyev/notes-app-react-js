import React, {createContext} from 'react';
import {Redirect} from "react-router";

const NotesContext = createContext();

export const NotesContextProvider = (props) => {
    const getNotes = async () => {
        return await fetch('http://localhost:3000/notes').then(result => result.json()).then(data => data);
    };

    const deleteNote = async (noteId) => {
        const requestOptions = {
            method: 'DELETE',
        };
        await fetch(`http://localhost:3000/notes/${noteId}`,requestOptions);
    };

    const updateNote = async (noteId, updatedNoteObject) => {
        const requestOptions = {
            method: 'PUT',
            headers: {'Content-Type': 'application/json'},
            body:JSON.stringify(updatedNoteObject)
        };
        return await fetch(`http://localhost:3000/notes/${noteId}`,requestOptions);
    };

    const addNote = async (newNote) => {
        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body:JSON.stringify(newNote)
        };
        return await fetch('http://localhost:3000/notes',requestOptions);
    }
    const getNote = async (id)=>{
        return await fetch('http://localhost:3000/notes/'+id).then(result=>result.json()).then(data=>data);
    }

    return (
        <NotesContext.Provider value={{getNotes:getNotes, addNote:addNote,updateNote:updateNote,getNote:getNote, deleteNote:deleteNote}}>
            {props.children}
        </NotesContext.Provider>
    )
};

export default NotesContext;