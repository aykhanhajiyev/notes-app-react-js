import React from 'react';
import {Link, Route} from 'react-router-dom'


const Note = (props) => {
    const id = props.noteObject.id;

    return (
        <>
            <Link to={`/notes/${id}`} style={{textDecoration: 'none'}}>
                <div className='note' style={{backgroundColor: props.noteObject.color}}>
                    <span className='note-title'> {props.noteObject.title} </span>
                    <span className='note-text'> {props.noteObject.text} </span>
                </div>
            </Link>

            {/*{// For now render Note in the Route below. When SingleNote is ready, render SingleNote.*/}
            {/*    <Route path={`/notes/${id}`} component={SingleNote}/>*/}
            {/*}*/}

        </>


    );
};

export default Note;