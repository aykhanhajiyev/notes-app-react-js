import React from 'react';
import {NotesContextProvider} from "./NotesContext";
import NoteList from "./NoteList";
import NoteForm from "./NoteForm";
import Header from "./Header";
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import SingleNote from "./SingleNote";


const App = () => {

    return (
            <NotesContextProvider>
                <Header/>
                <Switch>
                    <Route exact path={'/'} component={() => <NoteList filtering={'actual'}/>}/>
                    <Route exact path={'/actual'} component={() => <NoteList filtering={'actual'}/>}/>
                    <Route exact path={'/archive'} component={() => <NoteList filtering={'archive'}/>}/>
                    <Route path={'/create'} component={NoteForm}/>
                    <Route path={'/notes'} component={SingleNote}/>
                </Switch>
            </NotesContextProvider>

    )
}

export default App;
